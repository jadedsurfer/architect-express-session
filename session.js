"use strict";
var middleware = require("express-session");

module.exports = function(options, imports, register) {
  var debug = imports.debug("express:middleware:session");
  debug("start");

  debug(".useSetup");
  imports.express.useSetup(middleware(options));

  debug("register nothing");
  register(null, {});
};
